# Getting started

## Installation

- Run `npm install` to install all dependencies.

## Getting started

All the TypeScript source files are available inside the `./src` directory. These will be compiled to the `/lib` directory wiht the `tsc` (TypeScript Compiler) command.
This command is available with watch mode enabled from the npm scripts: `npm run dev`. 
This will under the hood start `./node_modules/.bin/tsc ---watch` using `tsconfig.json` as its configuration. 

So to start developing, run `npm run dev`.

## Tasks

### 1) Basic types, classes and interfaces

1) Design an `interface` for a bank account
2) Create two `classes` based off an `abstract` bank account `class` for a savings account and a checking account:
    - It has a balance
    - It can give (withdrawal) money
    - It can accept (deposit) money
    - The savings account also has an interest rate
3) Create a payment `class` with an `interface` which can transfer money from one account to another
4) Create a `Main.ts` file in which you can use instances of these classes and test them out

### 2) Generics

1) Create a queue `class` in which you can:
    - `push` instances of the payment `class` created in task 1.
    - `getNext` the first instance (using `[].shift`) from the queue so it can be consumed elsewhere 
2) Make this queue `class` more `generic` by allowing it to have a dynamic input type instead of just the payment `class`.

### 3) Decorators

1) Create a class which represents an ATM
    - It can withdrawal the requested amount of money when a correct PIN is given (with a `public withdrawal(amount: number, pin: number)` method for example)
2) Create a `class decorator` for the ATM in which you can configure the owner it can withdrawal (for example bills of 10, 50 and 100).
3) Create `method decorator` for the `withdrawal` method in which the `amount` and `pin` arguments are validated.
